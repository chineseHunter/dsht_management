import axios from 'axios'
import {Message} from 'element-ui';

const instance = axios.create({
    timeout: 15000,
    baseURL: 'http://kumanxuan1.f3322.net:8360'
})

//请求拦截器
instance.interceptors.request.use(result =>{
    console.log("请求拦截",result)
    return result
},err =>{
    return Promise.reject(err)
})

//响应拦截器
instance.interceptors.response.use(result =>{
    console.log("响应拦截",result)
    if(result.data.errno!==0){
        Message.error('登陆失败！请重新登录或者联系管理员');
    }
    return result.data; //重写它。只给里面的data
},err =>{
    return Promise.reject(err)
})

export default instance