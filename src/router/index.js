import Vue from 'vue'
import VueRouter from 'vue-router'
 
//调用后面参数的install
Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    component: () => import(/* webpackChunkName: "login" */ '../views/login/Login.vue')//懒加载
  },
  {
    path: '/',
    component: () => import(/* webpackChunkName: "homePage" */ '../views/homePage/HomePage.vue')//懒加载
  }
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  routes
})

export default router
